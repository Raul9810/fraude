package com.myspace.pruebasfraudes;

import java.io.Serializable;
import java.util.Date;
import com.myspace.pruebasfraudes.Persona;
import com.myspace.pruebasfraudes.Validacion;

public class Fraude implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3716807735569708654L;
	private Date fechaFraude;
	private int nivelFraude;
	private String nombreFraude;
	private Persona personaFraude;
	private Validacion validacionFraude;
	/**
	 * @return the fechaFraude
	 */
	public Validacion getValidacionFraude() {
		return validacionFraude;
	}
	/**
	 * @param fechaFraude the fechaFraude to set
	 */
	public void setValidacionFraude(Validacion validacionFraude) {
		this.validacionFraude = validacionFraude;
	}
	/**
	 * @return the fechaFraude
	 */
	public Date getFechaFraude() {
		return fechaFraude;
	}
	/**
	 * @param fechaFraude the fechaFraude to set
	 */
	public void setFechaFraude(Date fechaFraude) {
		this.fechaFraude = fechaFraude;
	}
	/**
	 * @return the persona
	 */
	public Persona getPersonaFraude() {
		return personaFraude;
	}
	/**
	 * @param persona the persona to set
	 */
	public void setPersonaFraude(Persona personaFraude) {
		this.personaFraude = personaFraude;
	}
	/**
	 * @return the nivelFraude
	 */
	public int getNivelFraude() {
		return nivelFraude;
	}
	/**
	 * @param nivelFraude the nivelFraude to set
	 */
	public void setNivelFraude(int nivelFraude) {
		this.nivelFraude = nivelFraude;
	}
	/**
	 * @return the nombreFraude
	 */
	public String getNombreFraude() {
		return nombreFraude;
	}
	/**
	 * @param nombreFraude the nombreFraude to set
	 */
	public void setNombreFraude(String nombreFraude) {
		this.nombreFraude = nombreFraude;
	}
}
