package com.myspace.pruebasfraudes;

import java.io.Serializable;

public class Datos implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private int gastos;
	private int ingresos;
	private Persona persona;
	/**
	 * @return the gastos
	 */
	public int getGastos() {
		return gastos;
	}
	/**
	 * @param gastos the gastos to set
	 */
	public void setGastos(int gastos) {
		this.gastos = gastos;
	}
	/**
	 * @return the ingresos
	 */
	public int getIngresos() {
		return ingresos;
	}
	/**
	 * @param ingresos the ingresos to set
	 */
	public void setIngresos(int ingresos) {
		this.ingresos = ingresos;
	}
	/**
	 * @return the persona
	 */
	public Persona getPersona() {
		return persona;
	}
	/**
	 * @param persona the persona to set
	 */
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
