package com.myspace.pruebasfraudes;

import java.io.Serializable;

public class RequesFraude implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5094028223909039210L;
	private Datos datos;
	private Fraude fraude;
	/**
	 * @return the datos
	 */
	public Datos getDatos() {
		return datos;
	}
	/**
	 * @param datos the datos to set
	 */
	public void setDatos(Datos datos) {
		this.datos = datos;
	}
	/**
	 * @return the fraude
	 */
	public Fraude getFraude() {
		return fraude;
	}
	/**
	 * @param fraude the fraude to set
	 */
	public void setFraude(Fraude fraude) {
		this.fraude = fraude;
	}
}
