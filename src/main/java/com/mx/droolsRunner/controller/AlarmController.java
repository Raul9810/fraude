package com.mx.droolsRunner.controller;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.kie.server.client.KieServicesClient;

import com.mx.droolsRunner.config.MyConfigurationObject;
import com.mx.droolsRunner.dto.DeviceEvent;
import com.mx.droolsRunner.services.CentralAlarmService;
import com.myspace.pruebasfraudes.Datos;
import com.myspace.pruebasfraudes.Fraude;
import com.myspace.pruebasfraudes.Persona;

@RestController
@RequestMapping("fraude")
public class AlarmController {
	
	@Autowired
	MyConfigurationObject conf;
	
	@Autowired
	CentralAlarmService centralAlarm;
	
	@GetMapping(path="/containers")
	public void getContainers() {
		centralAlarm.listContainers(conf.initializeKieServerClient());
    }
	
	@GetMapping(path="/commands")
	public Fraude execute(@Valid @RequestBody Datos datos) {
		System.out.println("Ingresa al controller");
		Persona persona1 = new Persona();
		Fraude fraude = new Fraude();
		persona1.setCorreoPersona("");
		persona1.setId(0);
		persona1.setNombrePersona("");
		persona1.setValido(false);
		fraude.setPersonaFraude(persona1);
		
		return centralAlarm.executeCommands(datos, fraude,conf.initializeKieServerClient());
    }
}
