package com.mx.droolsRunner.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class MyConfigurationObject {

  private static final String URL = "http://172.17.0.3:8080/kie-server/services/rest/server";
  private static final String USER = "kieserver";
  private static final String PASSWORD = "kieserver1!";

  private static final MarshallingFormat FORMAT = MarshallingFormat.JSON;

  private static KieServicesConfiguration conf;
  private static KieServicesClient kieServicesClient;

  public static KieServicesClient initializeKieServerClient() {
	  System.out.println("Se inicia la conexion");
      conf = KieServicesFactory.newRestConfiguration(URL, USER, PASSWORD);
      conf.setMarshallingFormat(FORMAT);
      kieServicesClient = KieServicesFactory.newKieServicesClient(conf);
      return kieServicesClient;
  }
}