package com.mx.droolsRunner.services;

import java.util.List;

import javax.annotation.PostConstruct;

import org.kie.server.client.KieServicesClient;
import org.kie.server.client.RuleServicesClient;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.KieServices;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.server.api.model.KieServerInfo;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.model.KieContainerResource;
import org.kie.server.api.model.KieContainerResourceList;
import org.kie.server.api.model.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myspace.pruebasfraudes.Datos;
import com.myspace.pruebasfraudes.Fraude;

import org.kie.api.command.BatchExecutionCommand;
import org.kie.internal.command.CommandFactory;
import org.drools.core.command.runtime.rule.GetObjectsCommand;


import org.kie.api.pmml.PMML4Result;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class CentralAlarmService {
	
	public void listContainers(KieServicesClient kieServicesClient) {
	    KieContainerResourceList containersList = kieServicesClient.listContainers().getResult();
	    List<KieContainerResource> kieContainers = containersList.getContainers();
	    System.out.println("Available containers: ");
	    for (KieContainerResource container : kieContainers) {
	        System.out.println("\t" + container.getContainerId() + " (" + container.getReleaseId() + ")");
	    }
	}
	
	public Fraude executeCommands(Datos datos, Fraude fraude,KieServicesClient kieServicesClient) {

	  String containerId = "PruebasFraudes_1.0.1-SNAPSHOT";
	  System.out.println("== Sending commands to the server ==");
	  RuleServicesClient rulesClient = kieServicesClient.getServicesClient(RuleServicesClient.class);
	  KieCommands commands = KieServices.Factory.get().getCommands();
	  
	  List<Command<?>> cmds = new ArrayList<Command<?>>();
	  cmds.add(commands.newInsert(datos));
	  cmds.add(commands.newInsert(fraude));
	  GetObjectsCommand getObjectsCommand = new GetObjectsCommand();
	    getObjectsCommand.setOutIdentifier("objects");

		
		cmds.add(commands.newFireAllRules());
		cmds.add(getObjectsCommand);
		BatchExecutionCommand myCommands = CommandFactory.newBatchExecution(cmds,
				"KSession21_1");
		System.out.println(myCommands);
		ServiceResponse<ExecutionResults> response = rulesClient.executeCommandsWithResults("PruebasFraudes_1.0.4-SNAPSHOT", myCommands);
				
		List responseList = (List) response.getResult().getValue("objects");
		
		System.out.println(responseList);
		return (Fraude) responseList.get(1);
//	  Command insert = commandsFactory.newInsert(datos);
//	  Command<?> insert2 = commandsFactory.newInsert(fraude);
//	  Command fireAllRules = commandsFactory.newFireAllRules();
//	  Command batchCommand = commandsFactory.newBatchExecution(Arrays.asList(insert,fireAllRules));
//	  System.out.println(batchCommand);
//	  ServiceResponse<String> executeResponse = rulesClient.executeCommands(containerId, batchCommand);
//	  System.out.println(executeResponse.getResult());
//	  System.out.println(executeResponse.getMsg());
//	  
//	  Command insert3 = commandsFactory.newGetObjects();
//	  Command batchCommand1 = commandsFactory.newBatchExecution(Arrays.asList(insert3,fireAllRules));
//	  System.out.println(batchCommand);
//	  ServiceResponse<String> executeResponse1 = rulesClient.executeCommands(containerId, batchCommand);
//	  System.out.println(executeResponse.getResult());
//	  System.out.println(executeResponse.getMsg());
	}
}
